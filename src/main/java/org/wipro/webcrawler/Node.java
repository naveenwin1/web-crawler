package org.wipro.webcrawler;

import java.util.ArrayList;
import java.util.List;

/**
 * Node representation of WEb links
 */
public class Node {

	private String link;

	private List<Node> childNodes;

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public List<Node> getChildNodes() {
		return childNodes;
	}

	public void setChildNodes(List<Node> childNodes) {
		this.childNodes = childNodes;
	}

	public void addNode(Node node){
		if(childNodes == null ){
			childNodes = new ArrayList<Node>();
		}
		childNodes.add(node);
	}

	@Override
	public String toString() {
		return "{" +
			   "\"link\": \"" + link + '\"' +
			   ", \"childNodes\" : " + childNodes +
			   "}";
	}
}
