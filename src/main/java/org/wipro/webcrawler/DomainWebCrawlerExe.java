package org.wipro.webcrawler;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 *
 * Class for crawling domain
 */
public class DomainWebCrawlerExe implements Runnable {

	private static final Set<String> visitedLinks = new HashSet<>();

	private static final Node rootNode = new Node();

	private String url;

	private Node node;

	private static ThreadPoolExecutor executor = new ThreadPoolExecutor(6, 12, 10L, TimeUnit.SECONDS, new LinkedBlockingDeque<Runnable>());

	public DomainWebCrawlerExe() {}

	public DomainWebCrawlerExe(String url, Node node) {
		this.url = url;
		this.node = node;
	}

	public static void main(String[] args) {

		String url = "http://wiprodigital.com";
		//Can be printed to a file or logger as per requirement
		System.out.println(new DomainWebCrawlerExe().crawl(url));

	}

	public String crawl(String rootDomain) {
		rootNode.setLink(rootDomain);
		visitedLinks.add(rootDomain);
		visitedLinks.add(rootDomain + "/");

		LocalDateTime timeStart = LocalDateTime.now();

		Runnable worker = new DomainWebCrawlerExe(rootDomain, rootNode);
		executor.execute(worker);

		//just waiting for getting at least 1 thread to be active
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		//Waiting to complete all threads
		while (executor.getActiveCount() != 0) {
		}
		executor.shutdown();

		System.out.println("TIME Taken to execute:" + ChronoUnit.SECONDS.between(timeStart, LocalDateTime.now()));

		return rootNode.toString();
	}


	private void processPage(String url, Node node) throws IOException {

		Connection con = Jsoup.connect(url);
		Document doc = con.get();
		Elements questions = doc.select("a[href]");

		//Looping anchor tags
		for (Element link : questions) {
			String href = link.attr("href");

			if (!visitedLinks.contains(href) && href.contains("http")) {
				Node childNode = new Node();
				childNode.setLink(href);
				node.addNode(childNode);
				visitedLinks.add(href);

				//Check if the URL is of same domain
				if (href.contains("http://wiprodigital.com")) {
					//Run in a different thread recursively
					Runnable worker = new DomainWebCrawlerExe(href, childNode);
					executor.execute(worker);
				}
			}
		}
	}

	@Override
	public void run() {
		try {
			processPage(url, node);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
