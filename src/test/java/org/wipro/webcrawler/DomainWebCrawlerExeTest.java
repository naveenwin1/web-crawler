package org.wipro.webcrawler;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import org.json.JSONException;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.AdditionalMatchers;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.skyscreamer.jsonassert.JSONAssert;


@RunWith(PowerMockRunner.class)
@PrepareForTest({Jsoup.class})
public class DomainWebCrawlerExeTest {


	/**
	 * Test to crawl single page with all the URL's
	 *
	 * @throws IOException
	 * @throws JSONException
	 */
	@Test
	public void testSinglePage() throws IOException, JSONException {

		Document docHome = Jsoup.parse(fileRead("/home.html"));
		Document docBlank = Jsoup.parse(fileRead("/blank.html"));

		String expectedResponse = fileRead("/response.json");

		Connection con1 = PowerMockito.mock(Connection.class);
		Connection con3 = PowerMockito.mock(Connection.class);

		PowerMockito.mockStatic(Jsoup.class);

		PowerMockito.when(Jsoup.connect(Matchers.eq("http://www.wiprodigital.com"))).
				thenReturn(con1);
		PowerMockito.when(Jsoup.connect(AdditionalMatchers.not(Matchers.eq("http://www.wiprodigital.com")))).
				thenReturn(con3);

		PowerMockito.when(con1.get()).thenReturn(docHome);
		PowerMockito.when(con3.get()).thenReturn(docBlank);

		DomainWebCrawlerExe crawler = new DomainWebCrawlerExe();

		String actualResponse = crawler.crawl("http://www.wiprodigital.com");
		System.out.println(actualResponse);
		JSONAssert.assertEquals(expectedResponse, actualResponse, false);

	}

	/**
	 * This is incomplete and does not work as expected due to multithreaded execution
	 *
	 * @param fileName
	 * @return
	 */
	/*@Test
	public void testNested() throws IOException, JSONException {

		Document docHome = Jsoup.parse(fileRead("/home.html"));
		Document docWhatWeDo = Jsoup.parse(fileRead("/whatWeDo.html"));
		Document docBlank = Jsoup.parse(fileRead("/blank.html"));

		String expectedResponse = fileRead("/responseNested.json");

		Connection con1 = PowerMockito.mock(Connection.class);
		Connection con2 = PowerMockito.mock(Connection.class);
		Connection con3 = PowerMockito.mock(Connection.class);

		PowerMockito.mockStatic(Jsoup.class);

		PowerMockito.when(Jsoup.connect(Matchers.eq("http://www.wiprodigital.com"))).
				thenReturn(con1);
		PowerMockito.when(Jsoup.connect(Matchers.eq("http://wiprodigital.com/what-we-do"))).
				thenReturn(con2);
		PowerMockito.when(Jsoup.connect(
				AdditionalMatchers.and(
						AdditionalMatchers.not(Matchers.eq("http://www.wiprodigital.com"))
						, AdditionalMatchers.not(Matchers.eq("http://wiprodigital.com/what-we-do")))
		)).thenReturn(con3);

		PowerMockito.when(con1.get()).thenReturn(docHome);
		PowerMockito.when(con2.get()).thenReturn(docWhatWeDo);
		PowerMockito.when(con3.get()).thenReturn(docBlank);

		DomainWebCrawlerExe crawler = new DomainWebCrawlerExe();

		String actualResponse = crawler.crawl("http://www.wiprodigital.com");

		JSONAssert.assertEquals(expectedResponse, actualResponse, false);

	}
*/
	private String fileRead(String fileName) {
		StringBuilder sb = new StringBuilder();

		try (InputStream stream = getClass().getResourceAsStream(fileName)) {

			new BufferedReader(new InputStreamReader(stream,
					StandardCharsets.UTF_8)).lines().forEach(sb::append);

		} catch (IOException e) {
			e.printStackTrace();
		}
		return sb.toString();
	}



}
