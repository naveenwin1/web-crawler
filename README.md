# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
    
   <p> 
       The web crawler application can crawl through hyperlinks by visiting all the links in the page within the given domain.
       It will give out as a json output which is tree structured format.
       Presently the applicaiton is hardcoded to work only on http://www.wiprodigital.com, this can be modified with a small change in code. 
       This will not visit any links which are not part of the domain but loggs them.
       The applciation runs in a multithreaded mode for better performanance.  
    
        Output for www.wiprodigital.com has been stored under main/resource/output.json for reference
        https://bitbucket.org/naveenwin1/web-crawler/src/ee8840b92e59b81e6693c3868acbe103a3e6c06f/src/main/resources/output.json?at=master&fileviewer=file-view-default
   </p>
   
* Version
    * 1.0-SNAPSHOT


### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
    * Maven 4.0
    * Java 8

* How to run tests
    * run command 'mvn clean package'
    * Open the project in IDE and run maven lifecycle clean compile and package
   
* Deployment instructions

* What could be done better
    * Store data in tgf format for better view of graph like structure
    * Reactive style of hitting the hyperlink using WebClient(Spring 5 reactor project)
    * Include more test cases

### Who do I talk to? ###

* Repo owner or admin

    <p>Naveen Kumar B<br>naveen.win1@gmail.com<p>
    
* Other community or team contact